package com.edward.login.misc;

/**
 * Created by Edward on 12/12/2014.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.edward.login.R;

import java.util.List;


public class PicasaArrayAdapter extends ArrayAdapter<PicasaEntry> {
    private ImageLoader mImageLoader;

    public PicasaArrayAdapter(Context context,
                              int textViewResourceId,
                              List<PicasaEntry> objects,
                              ImageLoader imageLoader
    ) {
        super(context, textViewResourceId, objects);
        mImageLoader = imageLoader;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.lv_picasa_row, null);
        }

        ViewHolder holder = (ViewHolder) v.getTag(R.id.id_holder);

        if (holder == null) {
            holder = new ViewHolder(v);
            v.setTag(R.id.id_holder, holder);
        }

        PicasaEntry entry = getItem(position);
        if (entry.getThumbnailUrl() != null) {
            holder.image.setImageUrl(entry.getThumbnailUrl(), mImageLoader);
        } else {
            holder.image.setImageResource(R.drawable.no_image);
        }

        holder.title.setText(entry.getTitle());

        return v;
    }


    private class ViewHolder {
        NetworkImageView image;
        TextView title;

        public ViewHolder(View v) {
            image = (NetworkImageView) v.findViewById(R.id.iv_thumb);
            title = (TextView) v.findViewById(R.id.tv_title);

            v.setTag(this);
        }
    }
}