package com.edward.login.misc;

/**
 * Created by Edward on 12/12/2014.
 */

/**
 * Holds the data for Picasa photo that is used in the {@link Act_NetworkListView}
 *
 * @author Ognyan Bankov
 *
 */
public class PicasaEntry {
    private String mTitle;
    private String mThumbnailUrl;


    public PicasaEntry(String title, String thumbnailUrl) {
        super();
        mTitle = title;
        mThumbnailUrl = thumbnailUrl;
    }


    public String getTitle() {
        return mTitle;
    }


    public String getThumbnailUrl() {
        return mThumbnailUrl;
    }
}